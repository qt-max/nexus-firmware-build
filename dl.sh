#!/bin/sh

set -e

cd "$(dirname "$0")"

git submodule update --init

wget -c 'https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip'
rm -rf sdk
mkdir sdk
cd sdk
unzip ../sdk-tools-linux-4333796.zip
cd -

mkdir -p factory
wget -c 'https://dl.google.com/dl/android/aosp/bullhead-opm7.181205.001-factory-5f189d84.zip' -P factory
wget -c 'https://dl.google.com/dl/android/aosp/bullhead-ota-opm7.181205.001-b1af2457.zip' -P factory

wget -c 'https://storage.googleapis.com/git-repo-downloads/repo'
chmod +x repo

mkdir -p android
cd android
mkdir -p .repo/local_manifests
cd .repo/local_manifests
rm -f *
ln -s ../../../local_manifests/* .
cd -
python2 ../repo init -u https://github.com/LineageOS/android.git -b lineage-15.1
python2 ../repo sync -c
