#!/bin/sh

set -e

cd "$(dirname "$0")"

PATCHDIR="$PWD/patches"

clean() {
	git checkout -- .
	git clean -fd
}

apply() {
	echo "Applying $1"
	patch --no-backup-if-mismatch -Np1 -i "$1"
}

cd android-prepare-vendor
clean
apply "$PATCHDIR/android-prepare-vendor-remove-verity-hack.patch"
cd -

cd android

cd bootable/recovery
clean
apply "$PATCHDIR/android_bootable_recovery-fix-for-lineage-external-e2fsprogs.patch"
cd -

cd build/make
clean
apply "$PATCHDIR/android_build_make-remove-install-tools-for-compatibility-with-lge-update-binary.patch"
cd -

cd device/lge/bullhead
clean
apply "$PATCHDIR/android_device_lge_bullhead-adapt-for-android-prepare-vendor.patch"
apply "$PATCHDIR/android_device_lge_bullhead-aosp-recovery.patch"
apply "$PATCHDIR/android_device_lge_bullhead-fdroid.patch"
apply "$PATCHDIR/android_device_lge_bullhead-opengapps.patch"
cd -

cd vendor/lineage
clean
apply "$PATCHDIR/android_vendor_lineage-reenable-recovery-flash.patch"
cd -

rm -rf vendor/fdroid
mkdir vendor/fdroid
cd vendor/fdroid
apply "$PATCHDIR/android_vendor_fdroid-packages.patch"
cd -

cd packages/apps/Dialer
clean
apply "$PATCHDIR/android_packages_apps_Dialer-call-recording-ukraine.patch"
cd -

cd -
