#!/bin/sh

set -e

cd "$(dirname "$0")"

BUILD_ID="opm7.181205.001"

cd android-prepare-vendor
./execute-all.sh -d bullhead -b "$BUILD_ID" -o ../android -i ../factory/bullhead-"$BUILD_ID"-factory-*.zip
cd -

cd android

# Use proprietary update-binary that supports lge_bullhead_update_gpt and lge_bullhead_recover_gpt commands
TMP_OUT=$(mktemp -d)
trap 'rm -rf $TMP_OUT' EXIT
unzip ../factory/bullhead-ota-"$BUILD_ID"-*.zip META-INF/com/google/android/update-binary -d "$TMP_OUT"
mv "$TMP_OUT"/META-INF/com/google/android/update-binary vendor/lge/bullhead/radio/update-binary
cat >> vendor/lge/bullhead/BoardConfigVendor.mk << "EOF"
TARGET_RELEASETOOL_OTA_FROM_TARGET_SCRIPT := ./build/tools/releasetools/ota_from_target_files -b vendor/lge/bullhead/radio/update-binary
EOF

# Fool the check in android-prepare-vendor
cat build/core/build_id.mk | sed -n '/^export BUILD_ID=/s/.*=//p' > vendor/lge/bullhead/build_id.txt
