#!/bin/bash

set -e
set -o pipefail

FIRMWARE_OUT="$(realpath "$1")"
cd "$(dirname "$0")/android"

[ -n "$FIRMWARE_OUT" ] || exit 1
mkdir "$FIRMWARE_OUT"

# ninja causes random build failures to happen.
export USE_NINJA=false

source build/envsetup.sh
export WITH_SU=true
mka clean
breakfast lineage_bullhead-userdebug
mka dist |& tee out/build.log
build/tools/releasetools/sign_target_files_apks -o -d ~/.android-certs out/dist/lineage_bullhead-target_files-*.zip "$FIRMWARE_OUT/signed-target_files.zip" |& tee out/sign.log
build/tools/releasetools/ota_from_target_files --block -k ~/.android-certs/releasekey -b vendor/lge/bullhead/radio/update-binary "$FIRMWARE_OUT/signed-target_files.zip" "$FIRMWARE_OUT/signed-ota_update.zip" |& tee out/ota.log
cp -t "$FIRMWARE_OUT" out/{build,sign,ota}.log out/host/linux-x86/bin/{adb,fastboot,simg2img}
